# TypeProperties  
  
The TypeProperties is an **experimental** project that should help you create better
TypeScript NodeJS Rest APIs. It enables you to create serializeable models that are JSON type
safe and fully type checked.

The TypeProperties is a solution for "writing type once", this is particularly useful when [Metadata Reflect API](https://www.npmjs.com/package/reflect-metadata)
is not enough, as Reflect is only coerce when types are solely based on `typeof` and `instanceof`.
In all the other cases when you wrap your type into an array or make it nullable with union types, Reflect stops preserving
types.
 
### Example usage  
  
```typescript  
import {  
  Properties,
  Property,
  Factory,
  Nullable,
  ListOf,
  Descr,
  Default,
} from '@type-properties/core';

enum UserStatus {
    ACTIVE = 0;
    INACTIVE = 1;
}


@Properties()  
export class UserProps extends UserIdentifierProperties {  
    
  @Descr('The user id')  
  id = Property[0](
    type => String,
  );  
  
  @Descr(`The user email`)  
  email = Property[3](
    type => Nullable(String),
  );  
    
  @Descr(`The user given name`)  
  givenName = Property[1](
    type => String,
  );  
    
  @Descr(`The user family name`)  
  familyName = Property[2](
    type => String,  
  );  
    
  displayName = Property[4](  
    type => String,  
    init => `${this.givenName} ${this.familyName}`,
  );  
    
  @Default(value => new Date()) 
  registeredAt = Property[5](
    type => Date,
  );  
    
  followers = Property[6](
    type => ListOf(User),
    init => [],
  );
  
  @Default(value => 'ACTIVE')
  status? = Property[7](
    type => UserStatus,
  );

}  
  
@Descr(`The user type`)  
export class User extends Factory(UserProps) {}  
  
const user = new User({  
  id: '1',  
  givenName: 'John',  
  familyName: 'Doe',  
});  
  
const parsedUser = User.fromJSONString(JSON.stringify(user));

if (
  parsedUser instanceof User
    && parsedUser instanceof UserProps,
    && parsedUser.registeredAt instanceof Date // witch means it unserializes Date objects
) {  
  // this will be true  
}
```  
  
##### TypeProperties gives you more abstraction to model types

```typescript
// Static members
export interface UserStatic {
  /**
   * The properties class
   */
  Properties: UserProperties;
  /**
   * The properties factory serializer
   */
  Serializer: JSONSerializer;

  /**
   * Get factory property definitions
   */
  getProperties(): ArrayLikePropertyDefinitions;

  /**
   * Get factory property names
   */
  getPropertyNames(): TupleKeys<UserProperties>;

  /**
   * Transform plain object to properties factory
   * @param obj
   */
  fromPlainObject(obj: PlainObject<UserProperties>): User;

  /**
   * Transform json serialized object into properties factory
   * @param json
   */
  fromJSON(json: { [key: string]: any } | any[]): User;

  /**
   * Transform json serialized string into properties factory
   * @param jsonString
   */
  fromJSONString(jsonString: any): User;

  /**
   * Transform property tuple values into properties factory
   * @param values
   */
  fromValues(values: TupleValues<UserProperties>): User;

  /**
   * Transform property tuple values into properties factory with strict values
   * @param values
   */
  fromStrictValues(values: StrictTupleValues<UserProperties>): User;

  /**
   * Encodes properties factory to base64 string
   * @param forge
   * @param encoding
   */
  stringify(forge: Forge<UserProperties>, encoding?: 'hex' | 'base64'): string;

  /**
   * Parse a base64 string into properties factory
   * @param str
   * @param encoding
   */
  parse(str: string, encoding?: 'hex' | 'base64'): User;

  /**
   * Encodes properties factory into array of bytes
   * @param forge
   * @param encoding
   */
  encode(forge: Forge<UserProperties>): Uint8Array;

  /**
   * Decodes array of bytes into properties factory
   * @param bytes
   */
  decode(bytes: Uint8Array): User;
}

// Prototype members
interface User {
  /**
   * Transform properties factory into tuple of property values
   */
  toValues(): TupleValues<UserProperties>;

  /**
   * Return all property keys
   */
  toKeys(): TupleKeys<UserProperties>;

  /**
   * Transform properties factory into plain object
   */
  toPlainObject(): PlainObject<UserProperties>;

  /**
   * Transform properties factory into json ready object
   */
  toJSON(): { [key: string]: any };

  /**
   * Transform properties factory into json ready tuple of values
   */
  toJSONValues(): any[];

  /**
   * Stringify
   * @param encoding
   */
  stringify(encoding?: 'hex' | 'base64'): string;

  /**
   * Transform serializeable factory into bytes
   */
  encode(): Uint8Array;
}
```