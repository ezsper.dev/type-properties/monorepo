import { FactoryClass } from '@type-properties/core';

export interface EncryptionStrategyOptions {
  encrypt(buf: Buffer): Buffer;
  decrypt(buf: Buffer): Buffer;
}

export function createEncryption(options: EncryptionStrategyOptions | (() => EncryptionStrategyOptions)) {
  const { encrypt, decrypt } = typeof options === 'function' ? options() : options;

  return Object.assign(
    (factory: FactoryClass) => {
      const _decode = (factory as any).decode;
      (factory as any).decode = function decode(this: any, bytes: any, encoding?: 'hex' | 'base64'): Buffer {
        if (typeof bytes === 'string') {
          return decode.call(this, Buffer.from(bytes, encoding || 'base64'));
        }
        const buf = bytes instanceof Buffer ? bytes : Buffer.from(bytes, encoding);
        return _decode.call(this, decrypt(buf));
      };

      const _encode = (factory as any).prototype.encode;
      (factory as any).prototype.encode = function encode(this: any, ...args: any[]): Buffer {
        const buf = _encode.apply(this, args);
        return encrypt(buf);
      };
    },
    {
      encrypt,
      decrypt,
    },
  );
}