/// <reference types="jest" />
import { Properties, Property, Factory, ListOf, Nullable } from '@type-properties/core';
import { Encryption } from './Encryption';

describe('Factory', () => {
  enum UserStatus {
    ACTIVE = 1,
    INACTIVE = 2
  }

  @Properties()
  class UserProps {

    id = Property[0](
      type => String,
    );

    email = Property[4](
      type => String,
    );

    givenName = Property[1](
      type => String,
    );

    familyName = Property[2](
      type => String,
    );

    displayName = Property[3](
      type => String,
      def  => `${this.givenName} ${this.familyName}`,
    );

    registeredAt = Property[5](
      type => Date,
      def  => new Date(),
    );

    status = Property[6](
      type => UserStatus,
      def  => 'ACTIVE',
    );

    followers? = Property[7](
      type => ListOf(User),
      def => [],
    );

    phoneNumber = Property[8](
      type => Nullable(String),
    );

  }

  const privateKey = 'byz9VFNtbRQM0yBODcCb1lrXtVVH3D3x';

  @Encryption(privateKey)
  class User extends Factory(UserProps) {}

  test('Forgebale must be instanceof Properties', () => {
    expect(User.Properties).toBe(UserProps);
    expect(User.prototype instanceof UserProps).toBe(true);
  });

  test('getPropertyNames must return in order', () => {
    expect(
      User.getPropertyNames(),
    ).toStrictEqual([
      'id',
      'givenName',
      'familyName',
      'displayName',
      'email',
      'registeredAt',
      'status',
      'followers',
      'phoneNumber',
    ]);
  });

  test('forge with defaults', () => {
    const user = new User({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      phoneNumber: null,
    });
    expect(user.displayName).toBe('John Doe');
  });

  test('forge without defaults', () => {
    const registeredAt = new Date();
    const user = new User({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      status: 'INACTIVE',
      phoneNumber: null,
    });
    user.followers = [
      new User({
        id: '2',
        email: 'another@another',
        givenName: 'Anna',
        familyName: 'Doe',
        phoneNumber: null,
      }),
    ];
    expect(user.displayName).toBe('Doe, John');
    expect(user.registeredAt).toBe(registeredAt);
    expect(user.status).toBe(UserStatus[UserStatus.INACTIVE]);
  });

  test('from values', () => {
    const registeredAt = new Date();
    const user = User.fromValues([
      '1',
      'John',
      'Doe',
      'Doe, John',
      'example@example',
      registeredAt,
      'INACTIVE',
      [],
      null,
    ]);
    expect(user.displayName).toBe('Doe, John');
    expect(user.registeredAt).toBe(registeredAt);
    expect(user.status).toBe(UserStatus[UserStatus.INACTIVE]);
  });

  test('from plain object', () => {
    const registeredAt = new Date();
    const user = User.fromPlainObject({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      status: 'INACTIVE',
      followers: [],
      phoneNumber: null,
    });
    expect(user.displayName).toBe('Doe, John');
    expect(user.registeredAt).toBe(registeredAt);
    expect(user.status).toBe(UserStatus[UserStatus.INACTIVE]);
  });


  test('from JSON', () => {
    const registeredAt = new Date();
    const user = new User({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      phoneNumber: null,
    });

    const parsedUser = User.fromJSONString(
      JSON.stringify(user),
    );
    expect(parsedUser.displayName).toBe('Doe, John');
    expect(parsedUser.registeredAt === registeredAt).toBe(false);
    expect(parsedUser.registeredAt).toStrictEqual(registeredAt);
    expect(parsedUser.status).toEqual(UserStatus[UserStatus.ACTIVE]);
  });

  test('stringify', () => {
    const registeredAt = new Date('2020-01-21T02:48:39.918Z');
    const user = new User({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      phoneNumber: null,
    });

    const stringified = user.stringify();
    const parsedUser = User.decode(stringified);
    expect(parsedUser.displayName).toBe('Doe, John');
    expect(parsedUser.registeredAt === registeredAt).toBe(false);
    expect(parsedUser.registeredAt).toStrictEqual(registeredAt);
    expect(parsedUser.status).toBe(UserStatus[UserStatus.ACTIVE]);
  });
});