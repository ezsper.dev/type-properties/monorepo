import {
  randomBytes,
  pbkdf2Sync,
  createCipheriv,
  createDecipheriv,
  BinaryLike,
} from 'crypto';
import { createEncryption } from '../createEncryption';

export function Encryption(privatekey: BinaryLike) {
  return createEncryption({
    encrypt(buf) {
      // random initialization vector
      const iv = randomBytes(16);

      // random salt
      const salt = randomBytes(64);

      // derive encryption key: 32 byte key length
      // in assumption the masterkey is a cryptographic and NOT a password there is no need for
      // a large number of iterations. It may can replaced by HKDF
      // the value of 2145 is randomly chosen!
      const key = pbkdf2Sync(privatekey, salt, 2145, 32, 'sha512');

      // AES 256 GCM Mode
      const cipher = createCipheriv('aes-256-gcm', key, iv);
      // encrypt the given text
      const encrypted = Buffer.concat([cipher.update(buf), cipher.final()]);
      // extract the auth tag
      const tag = cipher.getAuthTag();
      // generate output
      return Buffer.concat([salt, iv, tag, encrypted]);
    },
    decrypt(buf) {
      // convert data to buffers
      const salt = buf.slice(0, 64);
      const iv = buf.slice(64, 80);
      const tag = buf.slice(80, 96);
      const payload = buf.slice(96);

      // derive key using; 32 byte key length
      const key = pbkdf2Sync(privatekey, salt, 2145, 32, 'sha512');

      // AES 256 GCM Mode
      const decipher = createDecipheriv('aes-256-gcm', key, iv);
      decipher.setAuthTag(tag);

      // decrypt the payload
      return Buffer.concat([decipher.update(payload), decipher.final()]);
    },
  });
}