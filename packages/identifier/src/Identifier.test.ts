/// <reference types="jest" />
import { Properties, Property, Factory } from '@type-properties/core';
import { Identifier } from './Identifier';

describe('CRC16/ARC', () => {
  @Properties()
  class UserProps {
    id = Property[0](
      type => String,
    );
    displayName = Property[1](
      type => String,
    );

    givenName = Property[2](
      type => String,
    );

    familyName = Property[3](
      type => String,
    );
  }

  test('Must match', () => {

    @Identifier(16382)
    class User1 extends Factory(UserProps) {}

    const user = new User1({
      id: '1',
      displayName: 'John Doe',
      givenName: 'John',
      familyName: 'Doe',
    });

    const parse = Identifier.parse(user.stringify(), 'base64');
    expect(parse.type).toBe(16382);
    const decoded = User1.decode(user.encode());
    expect(decoded.id).toBe(user.id);
    expect(decoded.displayName).toBe(user.displayName);

    //expect(user.stringify('base64')).toBe('bb3d');
  });

  test('Must match', () => {

    @Identifier('User')
    class User2 extends Factory(UserProps) {}

    const user = new User2({
      id: '1567',
      displayName: 'Doe',
      givenName: 'John',
      familyName: 'Doe',
    });

    const parse = Identifier.parse(user.stringify(), 'base64');
    expect(parse.type).toBe('User');
    const decoded = User2.decode(user.encode());
    expect(decoded.id).toBe(user.id);
    expect(decoded.displayName).toBe(user.displayName);

    //expect(user.stringify('base64')).toBe('bb3d');
  });

  test('Must match', () => {

    @Identifier()
    class User3 extends Factory(UserProps) {}

    const user = new User3({
      id: 'sdkf32jfbfq',
      displayName: 'John',
      givenName: 'John',
      familyName: 'Doe',
    });
    const parse = Identifier.parse(user.stringify(), 'base64');
    expect(parse.type).toBe(User3.name);
    const decoded = User3.decode(user.encode());
    expect(decoded.id).toBe(user.id);
    expect(decoded.displayName).toBe(user.displayName);

    //expect(user.stringify('base64')).toBe('bb3d');
  });
});