import { FactoryClass } from '@type-properties/core/Factory';
import { crc16 } from './crc16';

export interface IdentifierParse {
  type: number | string;
  payload: Buffer;
  checksum: number;
}

function parse(bytes: string, encoding?: 'base64' | 'hex' | 'utf8'): IdentifierParse;
function parse(bytes: Uint8Array | Buffer): IdentifierParse;
function parse(bytes: Uint8Array | Buffer | string, encoding?: 'base64' | 'hex' | 'utf8') {
  let type: number | string;
  const buf = bytes instanceof Buffer
    ? bytes
    : typeof bytes === 'string'
      ? Buffer.from(bytes, encoding)
      : new Buffer(bytes);
  const isNumber = buf[0] >> 7;
  const isUint14 = buf[0] >> 6 & 1;
  buf[0] = buf[0] & 0x3f;
  let value: number;
  let skipBytes = 0;
  if (isUint14) {
    value = buf.readUInt16BE(0);
    skipBytes += 2;
  } else {
    value = buf.readUInt8(0);
    skipBytes += 1;
  }
  if (isNumber) {
    type = value;
  } else {
    type = buf.slice(isUint14 ? 2 : 1, value + 1).toString();
    skipBytes += value;
  }
  const checksum = buf.readUInt16BE(buf.length - 2);
  const payload = buf.slice(skipBytes, buf.length - 2);
  if (checksum != crc16(payload)) {
    throw new Error(`Checksum doesn't match`);
  }
  return { type, checksum, payload };
}

export const Identifier = Object.assign(
  function Identifier(type?: string | number) {
    return (factory: FactoryClass) => {
      const _decode = (factory as any).decode;
      (factory as any).decode = function decode(this: any, bytes: any, encoding?: 'hex' | 'base64'): Buffer {
        if (typeof bytes === 'string') {
          return decode.call(this, Buffer.from(bytes, encoding || 'base64'));
        }
        const buf = bytes instanceof Buffer ? bytes : Buffer.from(bytes, encoding);
        return _decode.call(this, parse(buf).payload);
      };

      const _encode = (factory as any).prototype.encode;
      (factory as any).prototype.encode = function encode(this: any, ...args: any[]): Buffer {
        const buf = _encode.apply(this, args);
        let typePrefix: Buffer;
        if (typeof type === 'number') {
          if (type < 0 || type % 1 !== 0) {
            throw new TypeError(`The identifier type as number must be an unsigned integer`);
          }
          if (type <= 63) {
            typePrefix = Buffer.alloc(1);
            typePrefix[0] = 0x80 | type;
          } else if (type <= 16383) {
            typePrefix = Buffer.alloc(2);
            typePrefix.writeUInt16BE(type, 0);
            typePrefix[0] = 0xc0 | typePrefix[0];
          } else {
            throw new TypeError(`The identifier type as number must not be higher than ${16383}`);
          }
        } else {
          const typeValue = Buffer.from(type || factory.name);
          let typeLen: Buffer;
          if (typeValue.byteLength <= 63) {
            typeLen = Buffer.alloc(1);
            typeLen[0] = typeValue.byteLength;
          } else if (typeValue.byteLength <= 16383) {
            typeLen = Buffer.alloc(2);
            typeLen.writeUInt16BE(typeValue.byteLength, 0);
            typeLen[0] = 0x40 | typeLen[0];
          } else {
            throw new TypeError(`The identifier type as string must have a byte length of ${16383} maximum`);
          }
          typePrefix = Buffer.concat([typeLen, typeValue]);
        }
        const checksum = Buffer.alloc(2);
        checksum.writeUInt16BE(crc16(buf), 0);
        return Buffer.concat([typePrefix, buf, checksum]);
      };
    };
  },
  {
    parse,
  },
);