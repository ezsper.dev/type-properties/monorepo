import {
  AbstractProperties,
  FindRepeatedIndex,
  IsAbstractClass,
  MakeTupleKey,
  MakeTupleKeySingle,
  PropertiesClass,
  AnyPropertyDefinitionOutput,
  ArrayLikePropertyDefinitions,
} from './typings';
import { computeProperties } from './Property';
import { __applyDecorators } from './decorators';

const $properties$ = Symbol.for('properties');

const ignoreProperty = ` ${[
  'id',
  'key',
  'default',
  'type',
].join(' ')} `;

export const __validateMetadata = {
  validate(Properties: PropertiesClass, property: AnyPropertyDefinitionOutput, name: string, key: string, value: any) {
    if (key === 'description' && typeof value !== 'string') {
      throw new Error(`The value for description must be a string`);
    }
  }
};

export function definePropertyMetadata<T extends PropertiesClass>(Properties: T, name: string, key: string, value: any) {
  if (ignoreProperty.indexOf(` ${key} `) >= 0) {
    throw new Error(`Cannot replace Key ${key}`);
  }
  const properties = getProperties(Properties);
  const property = properties[properties.keyMap[name]];
  if (typeof properties.keyMap[name] !== 'number') {
    throw new Error(`Property key ${name} was not found`);
  }
  __validateMetadata.validate(Properties, property, name, key, value);
  if (!property.metadata) {
    property.metadata = {};
  }
  property.metadata[key] = value;
}

export function getPropertyMetadata<T extends PropertiesClass>(Properties: T, name: string, key: string) {
  const properties = getProperties(Properties);
  for (let i = 0; i < properties.length; i++) {
    const property = properties[i];
    if (property.key === name) {
      if (property.hasOwnProperty(key)) {
        return property[key];
      }
      break;
    }
  }

  throw new Error(`Property key ${name} was not found`);
}

export function hasPropertyMetadata<T extends PropertiesClass>(Properties: T, name: string, key: string) {
  const properties = getProperties(Properties);
  for (let i = 0; i < properties.length; i++) {
    const property = properties[i];
    if (property.key === name) {
      if (property.hasOwnProperty(key)) {
        return true;
      }
      break;
    }
  }

  return false;
}

export function getProperties<C extends PropertiesClass>(target: C): ArrayLikePropertyDefinitions {
  if (target == null || target[$properties$] == null) {
    throw new Error(`Not decorated by Properties`);
  }
  return target[$properties$];
}

export function getProperty<C extends PropertiesClass>(target: C, propertyKey: string): AnyPropertyDefinitionOutput {
  const properties = getProperties(target);
  if (!properties.keyMap.hasOwnProperty(propertyKey)) {
    throw new Error(`Property ${propertyKey} is not a property like`);
  }
  return properties[properties.keyMap[propertyKey]];
}

export function hasProperty<C extends PropertiesClass>(target: C, propertyKey: string): boolean {
  const properties = getProperties(target);
  return properties.keyMap.hasOwnProperty(propertyKey);
}

export type PropertiesMustNotHaveArgs = { __ItMustNotHaveArgs?: undefined };
export type PropertiesMustNotBeAbstractClass = { __ItMustNotBeAnAbstractClass?: undefined };
export type PropertiesMustNotRepeatIndex<Index> = { __ItMustNotRepeatIndexes?: undefined };
export type EnsureProperties<T extends PropertiesClass> = IsAbstractClass<T> extends true
  ? PropertiesMustNotBeAbstractClass
  : MakeTupleKey<T['prototype']> extends MakeTupleKeySingle<T['prototype']>
    ? T extends new (...args: infer U) => T['prototype']
      ? U extends { length: 0 } ? T : PropertiesMustNotHaveArgs
      : T
    : PropertiesMustNotRepeatIndex<FindRepeatedIndex<T['prototype']>>;

/**
 * Decorator for testing and closing properties
 * @constructor
 */
export function Properties() {
  return <T extends Function & { prototype: AbstractProperties<T['prototype']> }>(target: EnsureProperties<T>): T => {
    const _target = function (this: any, ...args: any[]) {
      if (!(this instanceof _target)) {
        return new (_target as any)(...args);
      }
      const inst = new (target as any)();
      const self = this;
      Object.getOwnPropertyNames(inst).forEach(name => {
        Object.defineProperty(this, name, Object.getOwnPropertyDescriptor(inst, name) as any);
        const valueOf = () => inst[name].current;
        Object.defineProperty(inst, name, {
          value: Object.assign(valueOf, {
            valueOf,
            toString: () => valueOf().toString(),
          }),
        });
        Object.defineProperty(inst[name], 'current', {
          get() {
            if (self[name] != null) {
              return self[name];
            }
            const def = Reflect.getMetadata('DefaultValue', _target.prototype, name);
            return def ? def() : null;
          },
          set(value) {
            self[name] = value;
          },
        });
      });
      return this;
    };
    _target.prototype = (target as any).prototype;
    _target.prototype.constructor = _target;
    const properties = computeProperties(_target);
    try {
      require('reflect-metadata');
    } catch(err) {}

    if (typeof Reflect !== 'undefined' && typeof Reflect.defineMetadata !== 'undefined') {
      for (let i = 0; i < properties.length; i++) {
        const property = properties[i];
        try {
          Reflect.defineMetadata('design:type', property.type(), _target.prototype, property.key);
        } catch (error) {}
        try {
          Reflect.defineMetadata('@type-properties', property, _target.prototype, property.key);
        } catch (error) {}
      }
    }

    Object.defineProperty(_target, 'name', {
      enumerable: true,
      configurable: true,
      value: (target as any).name,
    });

    Object.defineProperty(_target, $properties$, {
      enumerable: false,
      value: properties,
    });

    Object.getOwnPropertyNames(target).forEach(name => {
      Object.defineProperty(_target, name, Object.getOwnPropertyDescriptor(target, name) as any);
    });

    __applyDecorators(target);
    return _target as any;
  };
}

/**
 * Check if value is a properties class
 * @param target
 */
export function isProperties(target: any): target is PropertiesClass {
  return target != null && target[$properties$] != null;
}