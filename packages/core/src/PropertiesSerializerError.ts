export class PropertiesSerializerError extends TypeError {

  originalError?: Error;
  extensions?: Record<string, any>;

  constructor(readonly code: string, message: string) {
    super(message);
    Object.setPrototypeOf(this, PropertiesSerializerError.prototype);
  }

}