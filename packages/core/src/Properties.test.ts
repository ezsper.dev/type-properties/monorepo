/// <reference types="jest" />
import { Properties, getProperties } from './Properties';
import { Property } from './Property';
import { Descr } from './decorators';
import { getDescription } from './utils';
import { Nullable } from './Nullable';

test('Empty properties class returns empty definitions', () => {
  @Properties()
  class SampleProps {

  }

  const properties = getProperties(SampleProps);
  expect(properties).toStrictEqual({ keyMap: {}, length: 0 });
});


test('Empty properties class returns empty definitions', () => {
  @Properties()
  class SampleProps {
    id = Property[0](
      type => String,
    );

    @Descr('The sample avatar')
    avatar = Property[1](
      type => Nullable(String),
      { myMeta: 'myMeta' }
    );

    @Descr('The sample avatar')
    fullName? = Property[2](
      type => String
    );
  }

  const properties = getProperties(SampleProps);
  expect(properties.length).toEqual(3);
  expect(properties[0].initialized).toEqual(false);
  expect(properties[0].type()).toEqual(String);
  expect(properties[1].initialized).toEqual(false);
  expect(properties[1].type()).toBeInstanceOf(Nullable);
  expect(properties[1].type().type).toBe(String);
  expect(properties[1].myMeta).toEqual('myMeta');
  expect(getDescription(SampleProps.prototype, 'avatar')).toEqual('The sample avatar');
});
