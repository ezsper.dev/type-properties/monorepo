export interface NullableStatic {

    <T>(type: T): NullableMember<T>;
    new <T>(type: T): NullableMember<T>;

}

export interface NullableMember<T> {
    __nullable: true;
    type: T;
}

export const Nullable = (function Nullable(this: NullableMember<any>, type: string) {
    if (!(this instanceof Nullable)) {
        return new (Nullable as NullableStatic)(type);
    }
    this.type = type;
    return this;
} as any) as NullableStatic;