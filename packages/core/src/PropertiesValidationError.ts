export class PropertiesValidationError extends TypeError {

  originalError?: Error;
  extensions?: Record<string, any>;

  constructor(readonly code: string, message: string) {
    super(message);
    Object.setPrototypeOf(this, PropertiesValidationError.prototype);
  }

}