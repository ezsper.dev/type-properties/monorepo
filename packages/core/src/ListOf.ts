export interface ListOfStatic {

    <T>(type: T): ListOfMember<T>;
    new <T>(type: T): ListOfMember<T>;

}

export interface ListOfMember<T> {
    __listOf: true;
    type: T;
}

export const ListOf = (function ListOf(this: ListOfMember<any>, type: string) {
    if (!(this instanceof ListOf)) {
        return new (ListOf as ListOfStatic)(type);
    }
    this.type = type;
    return this;
} as any) as ListOfStatic;