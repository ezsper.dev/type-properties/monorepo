import { Nullable, NullableMember } from './Nullable';
import { ListOf, ListOfMember } from './ListOf';

enum EnumCheck {}
const enumPrototype = Object.getPrototypeOf(EnumCheck);
export function isEnum(value: any): value is { [K in string | number]: string | number } {
  if (typeof value !== 'object'
    || value == null
    || Object.getPrototypeOf(value) !== enumPrototype) {
    return false;
  }
  for (const key in value) {
    if (value.hasOwnProperty(key) && value[value[key]] != key) {
      return false;
    }
  }
  return true;
}

export function isList<T>(type: NullableMember<ListOfMember<T>>): type is NullableMember<ListOfMember<T>>;
export function isList<T>(type: ListOfMember<T>): type is ListOfMember<T>;
export function isList(type: any): type is ListOfMember<any>;
export function isList(type: any): boolean {
  return type instanceof ListOf;
}

export function isNullable<T>(type: NullableMember<T>): type is NullableMember<T>;
export function isNullable(type: any): type is NullableMember<any>;
export function isNullable(type: any): boolean {
  return type instanceof Nullable;
}

export function getInnerType<T>(type: NullableMember<ListOfMember<NullableMember<T>>>): T;
export function getInnerType<T>(type: NullableMember<ListOfMember<T>>): T;
export function getInnerType<T>(type: ListOfMember<T>): T;
export function getInnerType<T>(type: NullableMember<T>): T;
export function getInnerType<T>(type: T): T;
export function getInnerType(type: any): any {
  return type instanceof Nullable || type instanceof ListOf
    ? getInnerType(type.type)
    : type;
}

export function getDescription(constructor: Function): string | null;
export function getDescription(prototype: object, key: string): string | null;
export function getDescription(...args: any[]) {
  return (Reflect.getMetadata as any)('description', ...args);
}