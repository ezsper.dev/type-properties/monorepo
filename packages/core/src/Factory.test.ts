/// <reference types="jest" />
import { Properties } from './Properties';
import { Property } from './Property';
import { Factory } from './Factory';
import { ListOf } from './ListOf';
import { Descr, Default } from './decorators';
import { getDescription } from './utils';
import { Nullable } from './Nullable';
import { Forge } from './typings';

describe('Factory', () => {
  enum UserStatus {
    ACTIVE = 1,
    INACTIVE = 2
  }

  @Properties()
  class UserProps {

    @Descr('The user id')
    id = Property[0](
      type => String,
    );

    @Descr('The user email')
    email = Property[4](
      type => String,
    );

    @Descr('The user given name')
    givenName = Property[1](
      type => String,
    );

    @Descr('The user family name')
    familyName = Property[2](
      type => String,
    );

    @Descr('The user display name')
    displayName? = Property[3](
      type => String,
      init => `${this.givenName.current} ${this.familyName.current}`,
    );

    @Default(value => new Date())
    @Descr('The user registration date')
    registeredAt? = Property[5](
      type => Date,
    );

    @Default(value => 'ACTIVE')
    @Descr('The user status')
    status? = Property[6](
      type => UserStatus,
    );

    @Descr('The user followers')
    followers? = Property[7](
      type => ListOf(User),
      init => [],
    );

    @Descr('The user phone number')
    phoneNumber = Property[8](
      type => Nullable(String),
    );

    @Descr('The user was removed')
    @Default(value => false)
    removed? = Property[9](
      type => Boolean,
    );


    @Descr('The user followers')
    parent = Property[10](
      type => Nullable(User),
    );

  }

  @Descr('The global user')
  class User extends Factory(UserProps) {}

  test('Forgebale must be instanceof Properties', () => {
    expect(User.Properties).toBe(UserProps);
    expect(User.prototype instanceof UserProps).toBe(true);
  });

  test('getPropertyNames must return in order', () => {
    expect(
      User.getPropertyNames(),
    ).toStrictEqual([
      'id',
      'givenName',
      'familyName',
      'displayName',
      'email',
      'registeredAt',
      'status',
      'followers',
      'phoneNumber',
      'removed',
      'parent',
    ]);
  });

  test('getProperties must be callable', () => {
    const properties = User.getProperties();
    expect(getDescription(User.prototype, 'id')).toBe('The user id');
    expect(properties[4].key).toBe('email');
  });

  test('type must have description', () => {
    expect(getDescription(User)).toBe('The global user');
  });

  test('forge with defaults', () => {
    const user = new User({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      phoneNumber: null,
    });
    expect(user.displayName).toBe('John Doe');
  });

  test('forge without defaults', () => {
    const registeredAt = new Date();
    const user = new User({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      status: 'INACTIVE',
      phoneNumber: null,
    });

    user.followers = [
      new User({
        id: '2',
        email: 'another@another',
        givenName: 'Anna',
        familyName: 'Doe',
        phoneNumber: null,
      }),
    ];

    user.parent = new User({
      id: '2',
      email: 'another@another',
      givenName: 'Anna',
      familyName: 'Doe',
      phoneNumber: null,
    });

    expect(user.displayName).toBe('Doe, John');
    expect(user.registeredAt).toBe(registeredAt);
    expect(user.status).toBe(UserStatus[UserStatus.INACTIVE]);
  });

  test('from values', () => {
    const registeredAt = new Date();
    const user = User.fromValues([
      '1',
      'John',
      'Doe',
      'Doe, John',
      'example@example',
      registeredAt,
      'INACTIVE',
      [],
      null,
      false,
      null,
    ]);
    expect(user.displayName).toBe('Doe, John');
    expect(user.registeredAt).toBe(registeredAt);
    expect(user.status).toBe(UserStatus[UserStatus.INACTIVE]);
  });

  test('from plain object', () => {
    const registeredAt = new Date();
    const user = User.fromPlainObject({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      status: 'INACTIVE',
      followers: [],
      phoneNumber: null,
      removed: false,
      parent: null,
    });
    expect(user.displayName).toBe('Doe, John');
    expect(user.registeredAt).toBe(registeredAt);
    expect(user.status).toBe(UserStatus[UserStatus.INACTIVE]);
  });


  test('from JSON', () => {
    const registeredAt = new Date();
    const user = new User({
      id: '1',
      email: 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      phoneNumber: null,
    });

    const parsedUser = User.fromJSONString(
      JSON.stringify(user),
    );
    expect(parsedUser.displayName).toBe('Doe, John');
    expect(parsedUser.registeredAt === registeredAt).toBe(false);
    expect(parsedUser.registeredAt).toStrictEqual(registeredAt);
    expect(parsedUser.status).toEqual(UserStatus[UserStatus.ACTIVE]);
  });

  test('stringify', () => {
    const registeredAt = new Date('2020-01-21T02:48:39.918Z');
    const user = new User({
      id: '1',
      email: () => 'example@example',
      givenName: 'John',
      familyName: 'Doe',
      displayName: 'Doe, John',
      registeredAt,
      phoneNumber: null,
    });

    const stringified = user.stringify();
    const parsedUser = User.decode(stringified);
    expect(
      Buffer.from(stringified, 'base64').toString(),
    ).toBe('["1","John","Doe","Doe, John","example@example","2020-01-21T02:48:39.918Z","ACTIVE",[],null,false,null]');
    expect(parsedUser.displayName).toBe('Doe, John');
    expect(parsedUser.registeredAt === registeredAt).toBe(false);
    expect(parsedUser.registeredAt).toStrictEqual(registeredAt);
    expect(parsedUser.status).toBe(UserStatus[UserStatus.ACTIVE]);
  });
});


describe('FactoryMixin', () => {
  let calledMixin = false;

  @Properties()
  class NodeProps {

    id = Property[0](
      type => String,
    );

  }

  class Node extends Factory(NodeProps) {
    static myStaticProperty() {}

    constructor(forge: Forge<NodeProps>) {
      super(forge);
      calledMixin = true;
    }

    myMemberProperty() {}
  }

  @Properties()
  class UserProps extends NodeProps {

    displayName = Property[1](
      type => String,
    );

    registeredAt = Property[2](
      type => Date,
      init => new Date('2020-01-21T02:48:39.918Z'),
    );

  }

  class User extends Factory(Node, UserProps) {}

  test('Forgebale must be instanceof Properties', () => {
    expect(User.Properties).toBe(UserProps);
    expect(User.prototype instanceof UserProps).toBe(true);
    expect(User.myStaticProperty).toBe(Node.myStaticProperty);
    expect(User.prototype.myMemberProperty).toBe(Node.prototype.myMemberProperty);
  });

  test('Called mixin', () => {
    const user = new User({ id: '1', displayName: 'John' });
    expect(calledMixin).toBe(true);
    expect(user.myMemberProperty).toBe(Node.prototype.myMemberProperty);
    expect(user.toValues()).toStrictEqual(['1', 'John', new Date('2020-01-21T02:48:39.918Z')]);
  });
});