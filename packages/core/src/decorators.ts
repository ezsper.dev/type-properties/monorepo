import 'reflect-metadata';
import {PropertyLikely, RequiredProperty} from './typings';
import { FactoryClass } from './Factory';
import { definePropertyMetadata } from './Properties';
import { TupleIndexes } from './normalize-tuple';

const $decorate$ = Symbol.for('decorate');

// const $metadata$ = Symbol.for('$metadata$');

/**
 * Decorator for defining property metadata
 * @param key Metadata key
 * @param value Metadata value
 * @constructor
 */
export function PropertyMetadata(key: string, value: any) {
  return <Key extends string, T extends { [K in Key]?: PropertyLikely<T[K]> }>(target: T, propertyKey: Key) => {
    if (!Object.prototype.hasOwnProperty.call(target.constructor, $decorate$)) {
      Object.defineProperty(target.constructor, $decorate$, {
        enumerable: false,
        configurable: true,
        value: [],
      });
    }
    target.constructor[$decorate$].push((target: any) => definePropertyMetadata(target, propertyKey, key, value));
  };
}

/**
 * Description decorator for properties
 * @param comment
 * @constructor
 */
export function Descr(comment: string): {
  (constructor: FactoryClass): void;
  <Key extends string, T extends { [K in Key]?: (...args: any[]) => any }>(target: T, propertyKey: Key): void;
  <Key extends string, T extends { [K in Key]?: PropertyLikely<T[K]> }>(target: T, propertyKey: Key): void;
};
export function Descr(comment: string) {
  return (...args: any) => {
    if (typeof args[0] === 'function') {
      Reflect.defineMetadata('description', comment, args[0]);
    } else {
      Reflect.defineMetadata('description', comment, args[0], args[1]);
    }
  };
}

/**
 * Set a default value to property
 * @param value
 * @constructor
 */
export function Default<Default>(value: (value: never) => Default): {
  <Key extends string, T>(target: T extends { [K in Key]?: RequiredProperty<TupleIndexes, infer U, any> } ? (Default extends U ? T : { [K in Key]?: RequiredProperty<TupleIndexes, Default, any> }) : { [K in Key]?: RequiredProperty<TupleIndexes, Default, any> }, propertyKey: Key): void;
};

export function Default(defaultValue: any) {
  return (...args: any) => {
    if (typeof args[0] === 'function') {
      throw new Error(`DefaultValue decorator must be used only on property key`);
    } else {
      const [target, property] = args;
      Reflect.defineMetadata('DefaultValue', defaultValue, target, property);
    }
  };
}

export function __applyDecorators(target: any) {
  if (target[$decorate$]) {
    for (const apply of target[$decorate$]) {
      apply(target.prototype.constructor);
    }
  }
}