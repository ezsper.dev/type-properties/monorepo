import { NormalizeTuple, TupleIndexes } from './normalize-tuple';
import { NullableMember } from './Nullable';
import { ListOfMember } from './ListOf';

export interface ClassType<T = any> {
  new(...args: any[]): T;
}

export type Any = string | number | boolean | symbol | { [K in string | number | symbol]: any };

declare global {

  /**
   * Workaround for boolean not specific
   */
  export interface BooleanConstructor {
    __boolean__: true;
  }

}

type ResolveInnerType<T> = Any extends T
  ? Any
  : T extends (typeof String) | StringConstructor
    ? string
    : T extends (typeof Number) | NumberConstructor
      ? number
      : T extends BooleanConstructor
        ? boolean
        : T extends Function & { prototype: infer U }
          ? U
          : T extends { [x: number]: string }
            ? keyof T
            : T;

type ResolveNullableType<T> = T extends NullableMember<infer U>
  ? ResolveListOfType<U> | null
  : ResolveListOfType<T>;

 type ResolveListOfType<T> = T extends ListOfMember<infer U>
  ? ResolveNullableType<U>[]
  : ResolveInnerType<T>;

export type ResolveType<T> = ResolveNullableType<OmitDefinition<T>>;

export type ValueRef<T> = { (): T, current: T, valueOf(): T, toString(): string };
export type DefinitionPropertyRequired<N extends TupleIndexes = TupleIndexes, T = any, Enhance = {}> = { $property$?: Enhance & { id: N, type: T, required: true } };
export type DefinitionPropertyOptional<N extends TupleIndexes = TupleIndexes, T = any, Enhance = {}> = { $property$?: Enhance & { id: N, type: T, required: true, optional?: true } };
export type DefinitionPropertyNullable<N extends TupleIndexes = TupleIndexes, T = any, Enhance = {}> = { $property$?: Enhance & { id: N, type: T, nullable?: true } };

export type RequiredProperty<N extends TupleIndexes, T, Enhance = {}> = ValueRef<T> & DefinitionPropertyRequired<N, T, Enhance>;
export type OptionalProperty<N extends TupleIndexes, T, Enhance = {}> = ValueRef<T> & DefinitionPropertyOptional<N, T, Enhance>;
export type OptionalNullableProperty<N extends TupleIndexes, T, Enhance = {}> = ValueRef<T | null> & DefinitionPropertyOptional<N, T, Enhance>;
export type NullableProperty<N extends TupleIndexes, T, Enhance = {}> = ValueRef<T | null> & DefinitionPropertyNullable<N, T, Enhance>;


export type OmitDefinition<T> = Exclude<T, null | undefined> extends { $property$?: { type: infer U } }
  ? U
  : T;

export type IsProperty<T> = Exclude<T, null | undefined> extends never
  ? false
  : Exclude<T, null | undefined> extends { $property$?: {} }
    ? true
    : false;

export type PropertyKeys<T> = Exclude<{ [K in keyof T]-?: IsProperty<T[K]> extends true ? K : null }[keyof T], null>;

export type Thunk<T> = (() => T) | T ;
export type TupleValues<T> = NormalizeTuple<MakeTupleValue<T>>;
export type StrictTupleValues<T> = NormalizeTuple<MakeTupleValueStrict<T>>;

export type TupleKeys<T> = NormalizeTuple<MakeTupleKey<T>>;

export type PropertyLength<T> = TupleValues<T> extends { length: infer U }
  ? U extends number ? U : never
  : never;

export type PropertyObject<T> = { [K in PropertyKeys<T>]: OmitDefinition<T[K]> };

export type PropertyDefinitions<T> = { [K in PropertyKeys<T>]: Exclude<T[K], null | undefined> extends { $property$?: infer U }
  ? U
  : DefinitionPropertyOptional | DefinitionPropertyNullable | DefinitionPropertyRequired };

export interface ArrayLikePropertyDefinitions extends ArrayLike<AnyPropertyDefinitionOutput> {
  keyMap: { [key: string]: number };
}

type Merge<T> = { [K in keyof T]: { (args: T[K]): void } }[keyof T] extends { (args: infer U): void }
  ? { [K in keyof U]: U[K] }
  : {};

type MergeDeep<T> = { [K in keyof T]: { (args: T[K]): void } }[keyof T] extends { (args: infer U): void }
  ? { [K in keyof U]: keyof U[K] }
  : {};

type MergeAnother<T> = { [K in keyof T]: { (args: T[K]): void } }[keyof T] extends { (args: infer U): void }
  ? { [K in keyof U]: U[K] extends (infer F)[] ? F : never }
  : {};

export type TupleItemValue<T> = T extends { id: TupleIndexes, type: any }
  ? {
    [K2 in T['id']]: T extends { nullable?: true }
      ? (T['type'] | null | undefined)
      : T extends { optional?: true }
        ? T['type'] | undefined
        : T['type']
  }
  : {};

export type TupleItemValueStrict<T> = T extends { id: TupleIndexes, type: any }
  ? { [K2 in T['id']]: T extends { nullable?: true } ? (T['type'] | null) : T['type'] }
  : {};

export type MakeTupleValueStrict<T> = Merge<{ [K in keyof PropertyDefinitions<T>]: TupleItemValueStrict<PropertyDefinitions<T>[K]> }>;
export type MakeTupleValue<T> = Merge<{ [K in keyof PropertyDefinitions<T>]: TupleItemValue<PropertyDefinitions<T>[K]> }>;

export type TupleItemKey<T, K> = T extends { id: TupleIndexes }
  ? K extends string ? { [K2 in T['id']]: {[K2 in K]: null }  } : {}
  : {};

export type TupleItemKeySingle<T, K> = T extends { id: TupleIndexes }
  ? K extends string ? { [K2 in T['id']]: K[]  } : {}
  : {};

export type MakeTupleKey<T> = MergeDeep<{ [K in keyof PropertyDefinitions<T>]: TupleItemKey<PropertyDefinitions<T>[K], K> }>;
export type MakeTupleKeySingle<T> = MergeAnother<{ [K in keyof PropertyDefinitions<T>]: TupleItemKeySingle<PropertyDefinitions<T>[K], K> }>;

export type IsPropertyOptional<T> = T extends { $property$?: infer U }
    ? (Required<U> extends { optional?: true } ? true : false)
    : false;
export type IsPropertyNullable<T> = T extends { $property$?: infer U }
  ? (Required<U> extends { nullable?: true } ? true : false)
  : false;
export type OptionalInlinePropertyKeys<T> = Exclude<{ [K in PropertyKeys<T>]: undefined extends T[K] ? K : null }[PropertyKeys<T>], null>;
export type OptionalPropertyKeys<T> = Exclude<{ [K in PropertyKeys<T>]: IsPropertyOptional<T[K]> extends true ? K : null }[PropertyKeys<T>], null>;
export type NullablePropertyKeys<T> = Exclude<{ [K in PropertyKeys<T>]: IsPropertyNullable<Exclude<T[K], null | undefined>> extends true ? K : null }[PropertyKeys<T>], null>;

export type Forge<T> = {
  [K in OptionalInlinePropertyKeys<T> | OptionalPropertyKeys<T>]?: K extends NullablePropertyKeys<T> ? Thunk<OmitDefinition<T[K]> | null> : Thunk<OmitDefinition<T[K]> | null>
} & {
  [K in Exclude<NullablePropertyKeys<T>, OptionalPropertyKeys<T> | OptionalInlinePropertyKeys<T>>]?: Thunk<OmitDefinition<T[K]> | null>
} & {
  [K in Exclude<keyof PropertyObject<T>, OptionalPropertyKeys<T> | OptionalInlinePropertyKeys<T> | NullablePropertyKeys<T>>]: Thunk<OmitDefinition<T[K]>>
};
export type OptionalKeys<T, P extends string | number | symbol> = { [K in Exclude<keyof T, Exclude<keyof T, P>>]?: T[K] } & Omit<{ [K in keyof T]: T[K] }, P>;
export type ForgeWithOptional<T, P extends keyof Forge<T>> = OptionalKeys<Forge<T>, P>;
export type PlainObject<T> = {
  [K in OptionalInlinePropertyKeys<T>]-?: K extends NullablePropertyKeys<T> ? OmitDefinition<T[K]> | null : OmitDefinition<T[K]>
} & {
  [K in OptionalPropertyKeys<T>]: OmitDefinition<T[K]>
} & {
  [K in Exclude<NullablePropertyKeys<T>, OptionalPropertyKeys<T> | OptionalInlinePropertyKeys<T>>]: OmitDefinition<T[K]> | null
} & {
  [K in Exclude<keyof PropertyObject<T>, OptionalPropertyKeys<T> | OptionalInlinePropertyKeys<T> | NullablePropertyKeys<T>>]: OmitDefinition<T[K]>
};

export type PlainObjectDeep<T> = {
  [K in OptionalInlinePropertyKeys<T>]-?: K extends NullablePropertyKeys<T> ? PlainObjectDeepSelf<OmitDefinition<T[K]>, T> | null : PlainObjectDeepSelf<OmitDefinition<T[K]>, T>
} & {
  [K in OptionalPropertyKeys<T>]: PlainObjectDeepSelf<OmitDefinition<T[K]>, T>
} & {
  [K in Exclude<NullablePropertyKeys<T>, OptionalPropertyKeys<T> | OptionalInlinePropertyKeys<T>>]: PlainObjectDeepSelf<OmitDefinition<T[K]>, T> | null
} & {
  [K in Exclude<keyof PropertyObject<T>, OptionalPropertyKeys<T> | OptionalInlinePropertyKeys<T> | NullablePropertyKeys<T>>]: PlainObjectDeepSelf<OmitDefinition<T[K]>, T>
};

export type PlainObjectDeepSelf<T, Self> = T extends (infer U)[]
  ? PlainObjectDeepSelf<U, Self>[]
  : { (self: T): void } extends { (self: { toPlainObject(): PlainObject<Self> }): void }
    ? PlainObjectDeep<Self>
    : T extends { toPlainObject(): infer U }
      ? U
      : T;

export type PropertyLikely<T> = IsProperty<T> extends true ? any : PropertyDefinition;

export type Optionals<T> = { [K in OptionalPropertyKeys<T> | OptionalInlinePropertyKeys<T>]: PlainObject<T>[K] };

export type PropertyDefinition = { $property$: {} };
export type AbstractProperties<T> = { [K in Exclude<keyof T, '__propertyKeys'>]?: PropertyLikely<T[K]> };
export type PropertiesClass<T = {}> = Function & { prototype: AbstractProperties<T> }

export type IsAbstractClass<T extends { prototype: any }> = T extends { new(...args: any[]): any }
  ? false
  : true;

export type FindRepeatedIndex<T> = Exclude<{ [K in keyof MakeTupleKey<T>]: K extends keyof MakeTupleKeySingle<T> ? MakeTupleKey<T>[K] extends MakeTupleKeySingle<T>[K] ? null : [K, MakeTupleKeySingle<T>[K], Exclude<MakeTupleKey<T>[K], MakeTupleKeySingle<T>[K]>] : null }[keyof MakeTupleKey<T>], null>;


export type MergeArgs<T extends object[]> = T extends (infer U)[]
    ? { [K in keyof U]: { (arg: { [K2 in K]: U[K] }): never } } extends Record<string, (arg: infer U) => any>
      ? (U extends infer A ? { [K in keyof A] : A[K] }: never)
      : never
    : never;

export type EnhancerCallerRequired<T, C, Map extends {} = {}> = { enhance: { <T1 extends T, C1 extends C>(property: PropertyDefinitionRequiredOutput<T1, C1>): Map } } | (object & Exclude<Map, Function>);
export type EnhancerCallerNullableOptional<T, C, Map extends {} = {}> = { enhance: { <T1 extends T, C1 extends C>(property: PropertyDefinitionNullableOptionalOutput<T1, C1>): Map } } | (object & Exclude<Map, Function>);
export type EnhancerCallerOptional<T, C, Map extends {} = {}> = { enhance: { <T1 extends T, C1 extends C>(property: PropertyDefinitionOptionalOutput<T1, C1>): Map } } | (object & Exclude<Map, Function>);
export type EnhancerCallerNullable<T, C, Map extends {} = {}> = { enhance: { <T1 extends T, C1 extends C>(property: PropertyDefinitionNullableOutput<T1, C1>): Map } } | (object & Exclude<Map, Function>);

export type Enhancer<T, C, Map extends {} = {}> =
  EnhancerCallerRequired<T, C, Map>
  | EnhancerCallerOptional<T, C, Map>;

export type Enhance<T extends Enhancer<any, any, any>[]> = T extends Enhancer<any, any, infer U>[]
  ? MergeArgs<U[]>
  : {};

export interface PropertyDefine<N extends TupleIndexes> {
  (type: (type?: never) => StringConstructor, def: (def?: never) => string): OptionalProperty<N, string>;
  (type: (type?: never) => NumberConstructor, def: (def?: never) => number): OptionalProperty<N, number>;
  (type: (type?: never) => BooleanConstructor, def: (def?: never) => boolean): OptionalProperty<N, boolean>;
  <U>(type: (type?: never) => U, def: (def?: never) => ResolveType<U>): OptionalProperty<N, ResolveType<U>>;
  <U>(type: (type?: never) => NullableMember<U>, def: (def?: never) => ResolveType<U> | null): OptionalNullableProperty<N, ResolveType<U>>;
  <Enhance = {}>(type: (type?: never) => StringConstructor, def: (def?: never) => string, ...enhacers: EnhancerCallerOptional<string, StringConstructor, Enhance>[]): OptionalProperty<N, string, Enhance>;
  <Enhance = {}>(type: (type?: never) => NumberConstructor, def: (def?: never) => number, ...enhacers: EnhancerCallerOptional<number, NumberConstructor, Enhance>[]): OptionalProperty<N, number, Enhance>;
  <Enhance = {}>(type: (type?: never) => BooleanConstructor, def: (def?: never) => boolean, ...enhacers: EnhancerCallerOptional<boolean, BooleanConstructor, Enhance>[]): OptionalProperty<N, boolean, Enhance>;
  <U, Enhance = {}>(type: (type?: never) => U, def: (def?: never) => ResolveType<U>, ...enhacers: EnhancerCallerOptional<ResolveType<U>, U, Enhance>[]): OptionalProperty<N, ResolveType<U>, Enhance>;
  <U, Enhance = {}>(type: (type?: never) => NullableMember<U>, def: (def?: never) => ResolveType<U> | null, ...enhacers: EnhancerCallerNullableOptional<ResolveType<U>, U, Enhance>[]): OptionalNullableProperty<N, ResolveType<U>, Enhance>;
  <U, Enhance = {}>(type: (type?: never) => NullableMember<U>, ...enhacers: EnhancerCallerNullable<ResolveType<U>, U, Enhance>[]): NullableProperty<N, ResolveType<U>, Enhance>;
  <U, Enhance = {}>(type: (type?: never) => U, ...enhacers: EnhancerCallerRequired<ResolveType<U>, U, Enhance>[]): RequiredProperty<N, ResolveType<U>, Enhance>;
  <U>(type: (type?: never) => NullableMember<U>): NullableProperty<N, ResolveType<U>>;
  <U>(type: (type?: never) => U): RequiredProperty<N, ResolveType<U>>;
}

export type PropertyOutOfBoundConstraint = {
  start: 0;
  end: 99;
};

export type PropertyCaller = { [K in TupleIndexes]: PropertyDefine<K> } & {
  [key: number]: PropertyOutOfBoundConstraint;
};

export interface PropertyDefinitionRequiredOutput<T, C> {
  constructor: Function,
  key: string;
  type: () => C;
  id: number;
  initialized: false;
  defaultValue?: () => ResolveType<C>;
}

export interface PropertyDefinitionNullableOptionalOutput<T, C> {
  constructor: Function,
  key: string;
  type: () => NullableMember<C>;
  id: number;
  initialized: true;
}

export interface PropertyDefinitionOptionalOutput<T, C> {
  constructor: Function,
  key: string;
  type: () => C;
  id: number;
  initialized: true;
}


export interface PropertyDefinitionNullableOutput<T, C> {
  constructor: Function,
  key: string;
  type: () => NullableMember<C>;
  id: number;
  initialized: false;
}
export type PropertyDefinitionOutput<T = any, C = any> = PropertyDefinitionRequiredOutput<T, C>
  | PropertyDefinitionOptionalOutput<T, C>
  | PropertyDefinitionNullableOptionalOutput<T, C>
  | PropertyDefinitionNullableOutput<T, C>;

export type AnyPropertyDefinitionOutput<T = any, C = any> = PropertyDefinitionOutput<T, C> & {
  [key: string]: any;
};