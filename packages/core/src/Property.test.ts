/// <reference types="jest" />
import { Property, computeProperties } from './Property';
import { Nullable } from './Nullable';

test('Property nullable returns null', () => {
  expect(
    Property[0](
      type => Nullable(String),
    ),
  ).toBe(undefined);
});

test('Property optional returns callable default value', () => {
  expect(
    (Property[0](
      type => String,
      def => 'DefaultValue',
    ) as any as () => string)(),
  ).toBe('DefaultValue');
});

test('Property required returns null', () => {
  expect(
    Property[0](
      type => String,
    ),
  ).toBe(undefined);
});

test('Property required returns null', () => {
  const createProperties = () => {
    return {
      id: Property[0](
        type => String,
      ),
    };
  };

  const properties = computeProperties(createProperties);

  expect(properties.length).toBe(1);
});


test('Property required returns null', () => {
  const createProperties = () => {
    return {
      id: Property[0](
        type => String,
        init => '',
      ),
    };
  };

  const properties = computeProperties(createProperties);

  expect(properties.length).toBe(1);
});